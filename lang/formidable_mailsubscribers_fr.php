<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_inscription_liste' => 'Inscription à la newsletter : ',
	'info_inscription_listes' => 'Inscription aux newsletters : ',

	// T
	'traiter_mailsubscribe_titre' => 'Inscription à des listes de diffusion',
	'traiter_mailsubscribe_description' => 'Inscrire aux listes de diffusion du plugin Mailsubscribers',
	'traiter_mailsubscribe_champ_email_label' => 'Champ contenant l’Email',
	'traiter_mailsubscribe_champ_nom_label' => 'Champ contenant le nom',
	'traiter_mailsubscribe_champ_listes_label' => 'Champ contenant les listes de diffusion',
	'traiter_mailsubscribe_champ_notify_label' => 'Notification',
	'traiter_mailsubscribe_champ_notify_label_case' => 'Envoyer une notification à l’abonné⋅e',
	'traiter_mailsubscribe_champ_invite_email_from_label' => 'Expéditeur de l’invitation',
	'traiter_mailsubscribe_champ_invite_email_from_explication' => 'Laisser vide pour utiliser la valeur par défaut',
	'traiter_mailsubscribe_champ_invite_email_text_label' => 'Message personnalisé d’invitation',
	'traiter_mailsubscribe_champ_invite_email_text_explication' => 'Laisser vide pour utiliser le texte par défaut',

);
