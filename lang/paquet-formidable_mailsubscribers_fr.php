<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'formidable_mailsubscribers_description' => 'Ajout d\'un traitement aux formulaires formidables pour permettre l\'abonnement aux listes de diffusion du plugin Mailsubscribers.',
	'formidable_mailsubscribers_nom' => 'Formidable : abonnements à des listes de diffusion',
	'formidable_mailsubscribers_slogan' => 'Extension du plugin formidable',
);
