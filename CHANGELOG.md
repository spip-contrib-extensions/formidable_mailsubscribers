# Changelog

## 2.0.1 2023-09-17

### Fixed

- Ne pas notifier lorsque l'option "Notifier" n'est pas cochée

## 2.0.0 2023-03-18

### Added

- Compatibilité SPIP 4

### Removed

- Compatibilité SPIP 3 à 3.2

## 1.1.8 2022-05-31

### Added


- Compatibilité SPIP 4.1

### Changed

- Compatibilité avec formidable v5.2.0 qui utilise `json_encode()` pour enregistrer les saisies en BDD
- Corrections diverses de chaînes de langue

### Fixed

- Si une personne demande à ne s'inscrire à aucune liste, ne pas lui dire qu'elle a été inscrite à une liste
- Si une personne ne s'inscrit à aucune liste, ne pas mettre son email dans `spip_mailsubscribers`
