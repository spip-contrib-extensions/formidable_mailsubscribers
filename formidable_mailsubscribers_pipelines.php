<?php

/**
 * Plugin mailsubscribers
 *
 * @plugin     Formidable : abonnement à des listes de diffusion
 * @copyright  2017
 * @author     tcharlss
 * @licence    GNU/GPL
 * @package    SPIP\FormidableMailsubscribers\pipelines
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Modifier le tableau retourné par la fonction charger des formulaires
 *
 * On précharge l'email et le nom pour l'abonnement aux newsletters
 * dans les formulaires formidables avec le traitement adéquat activé.
 *
 * @note
 * On s'aligne sur le charger du formulaire `newsletter_subscribe`
 *
 * @see formulaires_newsletter_subscribe_charger_dist()
 *
 * @param array $flux
 * @return array
 */
function formidable_mailsubscribers_formulaire_charger($flux) {
	include_spip('formidable_fonctions');

	// Recherche des éventuels traitements
	if (isset($flux['data']['_formidable']['traitements'])) {
		$traitements = function_exists('formidable_deserialize') ? formidable_deserialize($flux['data']['_formidable']['traitements']) : unserialize($flux['data']['_formidable']['traitements']);
	} else {
		$traitements = [];
	}

	if (
		$flux['args']['form'] === 'formidable'
		&& is_array($traitements)
		&& in_array('mailsubscribe', array_keys($traitements))
		&& !(isset($flux['args']['args'][2]) && $flux['args']['args'][2])// argument 2 dans le tableau, 3 de l'appel à #FORMIDABLE_CHARGER, c'est la réponse qu'on précharge, dans ce cas garder les valeurs de la réponse, et ne pas prendre en session
	) {
		foreach (['email', 'nom'] as $champ) {
			$nom_champ = (isset($traitements['mailsubscribe']["champ_{$champ}_mailsubscribe"]) ? $traitements['mailsubscribe']["champ_{$champ}_mailsubscribe"] : null);
			if (
				isset($GLOBALS['visiteur_session'][$champ])
				&& $nom_champ
				&& !_request($nom_champ)
			) {
				$flux['data'][$nom_champ] = $GLOBALS['visiteur_session'][$champ];
			} elseif (
				isset($GLOBALS['visiteur_session']["session_$champ"])
				&& !_request($nom_champ)
			) {
				$flux['data'][$nom_champ] = $GLOBALS['visiteur_session']["session_$champ"];
			}
		}
	}

	return $flux;
}
