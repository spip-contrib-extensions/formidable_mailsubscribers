<?php

/**
 * Traitement abonnement à des listes de diffusion à la saisie d'un formulaire
 *
 * @note
 * On s'aligne sur le formulaire `newsletter_subscribe` :
 * - on ne permet de s'inscrire qu'aux listes ouvertes, même si la saisie permet d'afficher les fermées.
 * - mode non graceful
 *
 * @see formidable_mailsubscribers_formulaire_charger()
 *
 * @plugin     Formidable : abonnement à des listes de diffusion
 * @copyright  2017
 * @author     tcharlss
 * @licence    GNU/GPL
 * @package    SPIP\FormidableMailsubscribers\traiter\mailsubscribe
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Traitements
 *
 * @uses newsletter_subscribe_dist()
 * @see formulaires_newsletter_subscribe_traiter_dist()
 *
 * @param array $args
 *     Tableau associatif :
 *     - formulaire
 *     - options : noms des champs, etc.
 * @param array $retours
 *     Tableau associatif :
 *     - editable (bool)
 *     - formidable_afficher_apres (string) : `formulaire` | …
 *     - id_formulaire (int)
 *     - traitements (array) : liste des traitements effectués
 *     - message_ok (string)
 * @return array
 */
function traiter_mailsubscribe_dist($args, $retours) {

	$formulaire  = $args['formulaire'];
	$options     = $args['options'];
	$message_erreur = '';

	// Noms des champs configurés
	$champ_email  = (isset($options['champ_email_mailsubscribe']) ? $options['champ_email_mailsubscribe'] : null);
	$champ_nom    = (isset($options['champ_nom_mailsubscribe']) ? $options['champ_nom_mailsubscribe'] : null);
	$champ_listes = (isset($options['champ_listes_mailsubscribe']) ? $options['champ_listes_mailsubscribe'] : null);
	$email = _request($champ_email);
	$listes = _request($champ_listes);

	// Il faut un email et des listes cochées pour procéder
	if ($email && $listes) {
		include_spip('inc/config');
		include_spip('inc/nospam');
		include_spip('inc/filtres');
		include_spip('base/abstract_sql');
		include_spip('inc/mailsubscribers');

		$options_subscribe = [];

		// Possibilité d'avoir un champs_liste qui renvoie juste un string, si par on propose un choix "je m'inscrit à la liste/je ne m'inscrit pas à la liste
		if (!is_array($listes)) {
			$listes = [$listes];
		}

		// S'assurer que les listes choisies sont ouvertes
		$listes_dispo = mailsubscribers_listes(['status' => 'open']);
		$listes = array_intersect($listes, array_keys($listes_dispo));

		// Si la personne n'a choisi que des listes fermées, on n'inscrit à aucune liste, donc on n'inscrit nul part
		if (empty($listes)) {
			$retours['traitements']['newsletters'] = true;
			return $retours;
		}

		$options_subscribe['listes'] = $listes;
		// Langue par défaut lors de l'inscription : la langue courante dans la page
		$options_subscribe['lang'] = $GLOBALS['spip_lang'];
		// On permet aux désinscrits de se réinscrire
		$options_subscribe['graceful'] = false;
		// Le nom avec un fallback
		if ($nom = (_request($champ_nom) ? _request($champ_nom) : sql_getfetsel('nom', 'spip_auteurs', 'email = ' . sql_quote($email)))) {
			$options_subscribe['nom'] = $nom;
		}
		// Notification
		if (!empty($options['notify'])) {
			$options_subscribe['notify'] = true;
		} else {
			$options_subscribe['notify'] = false;
		}


		// Message de retour en fonction du double opt-in
		if (lire_config('mailsubscribers/double_optin', 0)) {
			$message_ok = _T('newsletter:subscribe_message_ok_confirm', ['email' => "<b>$email</b>"]);
		} else {
			$message_ok = _T('newsletter:subscribe_message_ok', ['email' => "<b>$email</b>"]);
		}

		// Une constante pour déclencher l'action après le post, systématiquement
		if (!defined('_FORMIDABLE_MAILSUBSCRIBERS_NO_SPAM_CONFIRM_HTML')) {
			define('_FORMIDABLE_MAILSUBSCRIBERS_NO_SPAM_CONFIRM_HTML', true);
		}

		// Si nospam, ne déclencher l'action qu'après le POST
		if (
			function_exists('nospam_confirm_action_html')
			&& !($retours['redirect'] ?? '')
			&& _FORMIDABLE_MAILSUBSCRIBERS_NO_SPAM_CONFIRM_HTML
		) {
			$message_ok .= ' ' . nospam_confirm_action_html('subscribe', "Subscribe $email " . json_encode($options_subscribe), [$email, $options_subscribe], 'newsletter/');
		// Soit go go go direct
		} else {
			$newsletter_subscribe = charger_fonction('subscribe', 'newsletter');
			if (!$ok = $newsletter_subscribe($email, $options_subscribe)) {
				$message_ok = '';
				$message_erreur = _T('mailsubscriber:erreur_technique_subscribe');
			}
		}

		// Indiquer que ces messages de retour concernent ce traitement
		$intro_message = singulier_ou_pluriel(count($listes), 'formidable_mailsubscribers:info_inscription_liste', 'formidable_mailsubscribers:info_inscription_listes');
		if ($message_ok && count($listes)) {
			$retours['message_ok'] .= $intro_message . $message_ok;
		}
		if ($message_erreur) {
			$sep = ($retours['message_erreur'] ? '<br>' : '');
			$retours['message_erreur'] .= $sep . $intro_message . $message_erreur;
		}
	}

	// noter qu'on a déjà fait le boulot, pour ne pas risquer double appel
	$retours['traitements']['newsletters'] = true;

	return $retours;
}
